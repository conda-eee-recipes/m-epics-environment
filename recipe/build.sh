#!/bin/bash

# Create the modules directory
mkdir -p "${EPICS_MODULES_PATH}"

# Build the EPICS module
# Override PROJECT and LIBVERSION
# - PROJECT can't be guessed from the working directory
# - If we apply patches, the version will be set to the username
# WARNING: the build fails when using "-jX"
make PROJECT=environment LIBVERSION=2.1.1
make PROJECT=environment LIBVERSION=2.1.1 install

mkdir -p $CONDA_PREFIX/etc/conda/activate.d
cat <<EOF > $CONDA_PREFIX/etc/conda/activate.d/epics_vars_env.sh
# Set the location of "module.Makefile"
export EPICS_ENV_PATH=\${EPICS_MODULES_PATH}/environment/2.1.1/\${BASE}/bin/\${EPICS_HOST_ARCH}

# Add iocsh to PATH
# Note that iocsh has to be in the PATH because it looks for require relative to its path...
# REQUIREDIR=\${IOCSHDIR%/*/*/*}
# WARNING! Modification of PATH in activate.d can't be undone in deactivate.d!
# See https://github.com/conda/conda/issues/3915
# This is a temporary solution as a proof of concept
export PATH="\${EPICS_ENV_PATH}:\${PATH}"

# Source the bash completions for requireExec
[ -f "\${EPICS_ENV_PATH}/requireExecCompletions" ] && source \${EPICS_ENV_PATH}/requireExecCompletions
EOF

mkdir -p $CONDA_PREFIX/etc/conda/deactivate.d
cat <<EOF > $CONDA_PREFIX/etc/conda/deactivate.d/epics_vars_env.sh
unset EPICS_ENV_PATH
EOF
