m-epics-environmnent conda recipe
=================================

Home: https://bitbucket.org/europeanspallationsource/m-epics-environment

Package license: GPLv3

Recipe license: BSD 2-Clause

Summary: EPICS Environment
